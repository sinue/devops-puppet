node 'precise64' {
	include ssh
	include sudoers
	include nginx
	include php5
	include mysql
	#include postgres
	#include wordpress

	exec { 'Update apt-get':
           command => '/usr/bin/apt-get update',
	}

	user {
		'ubuntu':
           ensure     => present,
           comment    => 'Ubuntu User',
           home       => '/home/ubuntu',
           managehome => true,
	}

	ssh_authorized_key {
		'ubuntu_ssh':
		user => 'ubuntu',
		type => 'rsa',
		key => 'public_key',
	}
}