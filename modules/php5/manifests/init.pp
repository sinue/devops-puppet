# Class: php5

class php5 {
	package { [ 'php5-cli',
               'php5-fpm',
               'php5-mysql', 
               'php-pear']:
        ensure => installed,
	} 
}