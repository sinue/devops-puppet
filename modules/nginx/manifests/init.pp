# Manage nginx webserver
class nginx {
    package { 'nginx':
        ensure => installed,
	} 
	service { 'nginx':
    	ensure  => running,
    	require => Package['nginx'],
	}
	file { '/etc/nginx/sites-enabled/default':
    	source => 'puppet:///modules/nginx/site.conf',
    	notify => Service['nginx'],
	}

	file { '/var/www/paxia/info.php':
		content => '<?php phpinfo(); ?>',
	}	

}
